function loadData() {
    var xhttp = new XMLHttpRequest();
    var min = document.getElementById('min').value;
    var max = document.getElementById('max').value;
    xhttp.onreadystatechange = function () {
        var json = this.responseText;
        var json_array = json.split('"');
        document.getElementById("demo").innerHTML = 'Status code: ' + this.status + '<br/>' + json_array[3];
    };
    xhttp.open("GET", "http://localhost:8080/api/v1/fizz-buzz?min=" + min + "&max=" + max, true);
    xhttp.send();
}