package com.alfresco.fizzbuzz.controllers;

import com.alfresco.fizzbuzz.dtos.ApiResponse;
import com.alfresco.fizzbuzz.services.FizzBuzzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/fizz-buzz")
@RestController
public class FizzBuzzController {

  private final FizzBuzzService fizzBuzzService;

  @Autowired
  public FizzBuzzController(FizzBuzzService fizzBuzzService) {
    this.fizzBuzzService = fizzBuzzService;
  }

  @GetMapping
  public ResponseEntity<ApiResponse> getFizzBuzz(@RequestParam Integer min, @RequestParam Integer max) {
    return new ResponseEntity<>(new ApiResponse(fizzBuzzService.generateOutput(min, max)), HttpStatus.OK);
  }
}
