package com.alfresco.fizzbuzz.dtos;

import lombok.Data;

@Data
public class ApiResponse {
    private String result;

    public ApiResponse(String result) {
        this.result = result;
    }
}
