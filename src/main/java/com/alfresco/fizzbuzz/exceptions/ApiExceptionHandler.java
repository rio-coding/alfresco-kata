package com.alfresco.fizzbuzz.exceptions;

import com.alfresco.fizzbuzz.dtos.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(InvalidParametersException.class)
    public ResponseEntity<ApiError> handleInvalidParametersException(InvalidParametersException e){
        return new ResponseEntity<>(new ApiError(e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NumberFormatException.class)
    public ResponseEntity<ApiError> handleNumberFormatException(NumberFormatException e){
        return new ResponseEntity<>(new ApiError("Could not cast parameters to Integer: " + e.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
