package com.alfresco.fizzbuzz.exceptions;

public class InvalidParametersException extends RuntimeException {
  private static final long serialVersionUID = -3576040532292680667L;

  public InvalidParametersException(String message) {
    super(message);
  }
}
