package com.alfresco.fizzbuzz.services;

import com.alfresco.fizzbuzz.exceptions.InvalidParametersException;

public interface FizzBuzzService {
  String FIZZ = "fizz";
  String BUZZ = "buzz";
  String FIZZ_BUZZ = "fizzbuzz";

  String generateOutput(int min, int max);

  default void validateParameters(int min, int max) {
    if (min < 0 || max < 0 || min > max) {
      throw new InvalidParametersException(
          "Parameters must be positive numbers and first one greater than second one");
    }
  }
}
