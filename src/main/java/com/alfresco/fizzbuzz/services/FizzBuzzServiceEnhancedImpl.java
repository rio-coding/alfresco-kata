package com.alfresco.fizzbuzz.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
@Primary
@Slf4j
public class FizzBuzzServiceEnhancedImpl implements FizzBuzzService {

  private static final String ALFRESCO = "alfresco";
  private static final String INTEGER = "integer";

  @Override
  public String generateOutput(int min, int max) {
    validateParameters(min, max);

    Map<String, Integer> map = generateInitialMap();

    StringBuilder sb = new StringBuilder();
    for (int i = min; i <= max; i++) {
      if (String.valueOf(i).contains("3")) {
        sb.append(ALFRESCO);
        map.merge(ALFRESCO, 1, Integer::sum);
      } else if (i % 15 == 0) {
        sb.append(FIZZ_BUZZ);
        map.merge(FIZZ_BUZZ, 1, Integer::sum);
      } else if (i % 3 == 0) {
        sb.append(FIZZ);
        map.merge(FIZZ, 1, Integer::sum);
      } else if (i % 5 == 0) {
        sb.append(BUZZ);
        map.merge(BUZZ, 1, Integer::sum);
      } else {
        sb.append(i);
        map.merge(INTEGER, 1, Integer::sum);
      }
      if (i < max) {
        sb.append(" ");
      }
    }
    sb.append("\n");
    sb.append(getStatistics(map));
    map.clear();
    return sb.toString();
  }

  private Map<String, Integer> generateInitialMap() {
    Map<String, Integer> map = new LinkedHashMap<>();
    map.put(FIZZ, 0);
    map.put(BUZZ, 0);
    map.put(FIZZ_BUZZ, 0);
    map.put(ALFRESCO, 0);
    map.put(INTEGER, 0);
    return map;
  }

  private String getStatistics(Map<String, Integer> map) {
    StringBuilder sb = new StringBuilder();
    map.forEach((k, v) -> sb.append(k).append(": ").append(v).append(" "));
    return sb.toString().trim();
  }
}
