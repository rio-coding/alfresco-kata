package com.alfresco.fizzbuzz.services;

import org.springframework.stereotype.Service;

@Service("fizzBuzzServiceImpl")
public class FizzBuzzServiceImpl implements FizzBuzzService {
  @Override
  public String generateOutput(int min, int max) {
    validateParameters(min, max);

    StringBuilder sb = new StringBuilder();
    for (int i = min; i <= max; i++) {
      if (i % 15 == 0) {
        sb.append(FIZZ_BUZZ);
      } else if (i % 3 == 0) {
        sb.append(FIZZ);
      } else if (i % 5 == 0) {
        sb.append(BUZZ);
      } else {
        sb.append(i);
      }
      if (i < max) {
        sb.append(" ");
      }
    }
    return sb.toString();
  }
}
