package com.alfresco.fizzbuzz.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class FizzBuzzControllerTest {

  private static final String ENDPOINT = "/api/v1/fizz-buzz";

  @Autowired private MockMvc mockMvc;

  @Test
  public void shouldReturnValidResultForValidRange1_20() throws Exception {
    String params = "?min=1&max=20";
    String expected =
        "1 2 alfresco 4 buzz fizz 7 8 fizz buzz 11 fizz alfresco 14 fizzbuzz 16 17 fizz 19 buzz\nfizz: 4 buzz: 3 fizzbuzz: 1 alfresco: 2 integer: 10";
    mockMvc
        .perform(get(ENDPOINT + params).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.result").value(expected));
  }

  @Test
  public void shouldReturnErrorMessageForNegativeParams() throws Exception {
    String params = "?min=-1&max=20";
    String expected = "Parameters must be positive numbers and first one greater than second one";
    mockMvc
        .perform(get(ENDPOINT + params).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.error").value(expected))
        .andExpect(jsonPath("$.result").doesNotExist());
  }

  @Test
  public void shouldReturnErrorMessageForIncorrectParams() throws Exception {
    String params = "?min=0&max=invalidInput";
    String expected = "Could not cast parameters to Integer: For input string: \"invalidInput\"";
    mockMvc
        .perform(get(ENDPOINT + params).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.error").value(expected))
        .andExpect(jsonPath("$.result").doesNotExist());
  }
}
