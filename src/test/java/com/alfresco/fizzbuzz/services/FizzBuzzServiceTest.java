package com.alfresco.fizzbuzz.services;

import com.alfresco.fizzbuzz.exceptions.InvalidParametersException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FizzBuzzServiceTest {

  @Autowired
  @Qualifier("fizzBuzzServiceImpl")
  FizzBuzzService fizzBuzzService;
  
  @Autowired FizzBuzzService fizzBuzzServiceEnhanced;

  
  @Test
  public void generateOutputShouldReturnValidStringForValidInterval(){
    String expected = "1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz 16 17 fizz 19 buzz";
    String actual = fizzBuzzService.generateOutput(1,20);
    
    Assert.assertEquals(expected, actual);
  }

  @Test
  public void enhancedGenerateOutputShouldReturnValidStringForValidInterval1_20() {
    String expected =
        "1 2 alfresco 4 buzz fizz 7 8 fizz buzz 11 fizz alfresco 14 fizzbuzz 16 17 fizz 19 buzz\n"
            + "fizz: 4 buzz: 3 fizzbuzz: 1 alfresco: 2 integer: 10";
    String actual = fizzBuzzServiceEnhanced.generateOutput(1, 20);

    Assert.assertEquals(expected, actual);
  }

  @Test
  public void enhancedGenerateOutputShouldReturnValidStringForValidInterval5_10() {
    String expected =
            "buzz fizz 7 8 fizz buzz\n"
                    + "fizz: 2 buzz: 2 fizzbuzz: 0 alfresco: 0 integer: 2";
    String actual = fizzBuzzServiceEnhanced.generateOutput(5, 10);

    Assert.assertEquals(expected, actual);
  }

  @Test(expected = InvalidParametersException.class)
  public void enhancedGenerateOutputShouldThrowExceptionWhenFirstParamIsNegative() {
    fizzBuzzServiceEnhanced.generateOutput(-1, 10);
  }

  @Test(expected = InvalidParametersException.class)
  public void enhancedGenerateOutputShouldThrowExceptionWhenSecondParamIsNegative() {
    fizzBuzzServiceEnhanced.generateOutput(10, -1);
  }

  @Test(expected = InvalidParametersException.class)
  public void enhancedGenerateOutputShouldThrowExceptionWhenFirstParamIsBigger() {
    fizzBuzzServiceEnhanced.generateOutput(20, 10);
  }
}
